# Teste Devops - Smarttbot

Dentro deste repositório você irá encontrar duas aplicações, uma escrita em Go (Backend) e outra em React (Frontend).

As aplicações compõem um sistema simples de chat que utiliza o Redis (https://redis.io/) para armazenamento das mensagens.

Você deverá criar um repositório no Git de sua preferência (Gitlab, Github, Bitbucket, etc.) com o conteúdo deste repositório e as alterações que julgar necessárias para a criação de dois ambientes distintos:

- Desenvolvimento - Utilizando docker e docker-compose
- Produção - Utilizando docker e uma ferramenta de CI/CD para automatizar o build e push das imagens docker para o DockerHub. Você pode escolher uma das seguintes ferramentas:
  - CircleCI
  - GitlabCI
  - TravisCI
  - Jenkins

Além disso, você deverá documentar seu trabalho da melhor maneira possível. Quando finalizar, é só enviar o link do seu repositório pra gente.

> OBS: As documentações das aplicações são propositalmente vagas, então recorra à leitura do código em caso de dúvidas sobre o funcionamento.

# Projeto

## Topologia lógica
- Ver "Topologia Lógica.PNG" no projeto

## Entendimento dos requisitos
- docker-compose apenas para Desenvolvimento
- Ambiente local para Desenvolvimento obrigatoriamente
- Configuração restrito em docker para Produção
- CI como repositório de Desenvolvimento
- CI/CD para ambiente de Produção apenas

## to do
- Entender melhor react para corrigir apontamentos do frontend

## doing


## done
- Conteinerizar app frontend
- Conteinerizar app backend
- Subir terraform com vpc e vm de Produção
- Configurar runner Desenvolvimento e Produção no projeto
- Criar repositório público no docker hub que atenda frontend e backend
- Criar estágio pipeline Produção - build
- Criar estágio pipeline Produção - test
- Criar estágio pipeline Produção - deploy
- Subir container do redis

# Ambiente de dev
- pc local
http://<ip local do host>:3000

# Ambiente de prd
- EC2 com terraform e docker
- Acessível em http://<ip público do host>:3000

# Melhorias
- Subir ambientes de Desenvolvimento e Produção em EC2, retirando Desenvolvimento do pc local
- Separar frontend e backend em projetos distintos no gitlab

# Dificuldades encontradas
- Entender melhor sobre node e react para conteinerizar a aplicação no frontend
- Entender melhor de go para conteinerizar a aplicação no backend

# Aplicação do projeto
- Configurar aws cli e autenticação
- Configurar terraform
- Acessar pasta terraform e aplicar o terraform
- Configuração do runner em produção
- Realizar autenticação do docker na vm do runner para repositório
- Criar rede interna docker no runner
- Rodar a pipeline
- Acessar aplicação em http://<ip público do host>:3000