resource "aws_vpc" "vpc-prd" {
  cidr_block       = var.main_vpc_cidr
  instance_tenancy = "default"
  tags = {
    "Name"   = "vpc-hacka"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_subnet" "sub-tf-pub-prd" {
  vpc_id            = aws_vpc.vpc-prd.id
  cidr_block        = var.public_subnets
  availability_zone = "${var.region}a"
  tags = {
    "Name"   = "sub-tf-pub-prd"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_internet_gateway" "igw-prd" {
  vpc_id = aws_vpc.vpc-prd.id
  tags = {
    "Name"   = "igw-prd"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_route_table" "rtb-prd" {
  vpc_id = aws_vpc.vpc-prd.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-prd.id
  }
  tags = {
    "Name"   = "rtb-prd"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_route_table_association" "public-rtb-association" {
  subnet_id      = aws_subnet.sub-tf-pub-prd.id
  route_table_id = aws_route_table.rtb-prd.id
}

# resource "aws_eip" "nateIP" {
#   vpc = true
# }

###
# Public Security Group
##

resource "aws_security_group" "ec2-sg" {
  name        = "tf-ec2-sg"
  description = "Acesso publico"
  vpc_id      = aws_vpc.vpc-prd.id

  tags = {
    "Name" = "tf-ec2-sg"
    "Role" = "public"
    # Project     = "dev"
    # Environment = "dev"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_security_group_rule" "public_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "saida default"
}

resource "aws_security_group_rule" "public_in_interno" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = [var.main_vpc_cidr]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "acesso total da rede dentro da mesma vpc"
}

# resource "aws_security_group_rule" "public_in_ssh" {
#   type              = "ingress"
#   from_port         = 22
#   to_port           = 22
#   protocol          = "tcp"
#   cidr_blocks       = [var.public_ip_dyego]
#   security_group_id = aws_security_group.ec2-sg.id
#   description       = "acesso ssh dyego"
# }

resource "aws_security_group_rule" "public_in_ssh" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = [var.public_ip_dyego]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "acesso total dyego"
}

###
# Public Security Group
##