# VPC
output "vpc_name" {
  value = aws_vpc.vpc-prd.tags.Name
}
output "sub_name" {
  value = aws_subnet.sub-tf-pub-prd.tags.Name
}
output "sub_id" {
  value = aws_subnet.sub-tf-pub-prd.id
}