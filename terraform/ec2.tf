resource "aws_key_pair" "chave-tf-1" {
  key_name   = "chave-tf-1"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDO+kBKNX1XRahpE+Oa0cydV8fnb72fXLGt4h3+aMoX7Mm/7sfs650zI18H4GI5RtN8ZL45YwZIsl6NzKtkrxeOx1f40WncRihqIuBVdqAfruPnPeHyF1mVQSbc3Vjqi8vG8XjIUfIvmWFBzuQNaCd/x1EKT/lncxb555jOo7uFDr5kYWnQ78hWExsDyW/Ngcfxwy+IC/kWWRh65Xbf/2mIZ+94MIeYYC4L5Ux44fJy3uk54BUIIRSSpmDyKH/IAasyX2U3nBviI3/A5VI96nTfVnf5Es4vTjqGE98cpYyHF0G3x/ZCKZ05oeim+QjuEM4X/c39ZdKRIim95XB6YW8J ubuntu"
}

resource "aws_instance" "ec2-vm01" {
  ami                         = "ami-0eea504f45ef7a8f7" #ubuntu server 20.04 us-east-2
  instance_type               = "c6a.xlarge"
  key_name                    = aws_key_pair.chave-tf-1.key_name
  subnet_id                   = aws_subnet.sub-tf-pub-prd.id
  associate_public_ip_address = true
  private_ip                  = "10.83.1.201"
  vpc_security_group_ids      = [aws_security_group.ec2-sg.id]
  user_data                   = file("script-inicial.sh")
  tags = {
    "Name"   = "ec2-vm01"
    "deploy" = "terraformdeploy"
  }
}