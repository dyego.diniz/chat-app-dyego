variable "region" {
  type        = string
  description = "regiao da aws"
  default     = "us-east-2"
}

variable "main_vpc_cidr" {
  type        = string
  description = "vpc cidr"
  default     = "10.83.0.0/16"
}

variable "public_subnets" {
  type        = string
  description = "cidr publico"
  default     = "10.83.1.0/24"
}

variable "public_ip_dyego" {
  type        = string
  description = "ip público Dyego"
  default     = "201.7.35.76/32"
}